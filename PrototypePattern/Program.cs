﻿using PrototypePattern.Models;

ShowShallowCloneResult();
ShowDeepCloneResult();
Console.ReadLine();

Member GetMemberSample() =>  new()
{
    Name = "Sample",
    Age = 30,
    Address = new Address { Street = "Sample St." }
};

void ShowShallowCloneResult()
{
    var member = GetMemberSample();
    Console.WriteLine("Member:" + member);
    var memberShallowClone = member.ShallowClone();
    Console.WriteLine("Shallow clone before editing:" + memberShallowClone);
    memberShallowClone.Name = "Mary";
    memberShallowClone.Age = 20;
    if (memberShallowClone.Address is null) return;;
    memberShallowClone.Address.Street = "Shallow St.";
    Console.WriteLine("Shallow clone after editing:" + memberShallowClone);
    Console.WriteLine("Member:" + member);
    Console.WriteLine();
}

void ShowDeepCloneResult()
{
    var member = GetMemberSample();
    Console.WriteLine("Member:" + member);
    var memberDeepClone = member.DeepClone();
    Console.WriteLine("Deep clone before editing:" + memberDeepClone);
    memberDeepClone.Name = "Joe";
    memberDeepClone.Age = 26;
    if (memberDeepClone.Address is null) return;
    memberDeepClone.Address.Street = "Deep St.";
    Console.WriteLine("Deep clone after editing:" + memberDeepClone); 
    Console.WriteLine("Member:" + member);
}