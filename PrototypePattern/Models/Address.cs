namespace PrototypePattern.Models;

public class Address
{
    public string? Street { get; set; }
}