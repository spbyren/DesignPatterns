using System.Text.Json;

namespace PrototypePattern.Models;

public class Member
{
    public string? Name { get; set; }
    public int Age { get; set; }
    public Address? Address { get; set; }

    public Member ShallowClone() => (Member)MemberwiseClone();

    public Member DeepClone()
    {
        var member = (Member)MemberwiseClone();
        if (member.Address != null)
            member.Address = new Address { Street = Address?.Street };
        return member;
    }

    public override string ToString() => JsonSerializer.Serialize(this);
}