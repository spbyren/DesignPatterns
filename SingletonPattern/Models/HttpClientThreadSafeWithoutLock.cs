namespace SingletonPattern.Models;

public sealed class HttpClientThreadSafeWithoutLock
{
    private static readonly HttpClient HttpClient = new();

    public static HttpClient Instance => HttpClient;
}