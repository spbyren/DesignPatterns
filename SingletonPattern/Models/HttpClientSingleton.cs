namespace SingletonPattern.Models;

public sealed class HttpClientSingleton
{
    private static HttpClient? _httpClient;

    public static HttpClient Instance
    {
        get
        {
             _httpClient ??= new();
            return _httpClient;
        }
    }
}