namespace SingletonPattern.Models;

public sealed class HttpClientThreadSafeSingleton
{
    private static HttpClient? _httpClient;
    private static readonly object LockObject = new();

    public static HttpClient Instance
    {
        get
        {
            if (_httpClient is null)
            {
                lock (LockObject) _httpClient ??= new();
            }
            return _httpClient;
        }
    }
}