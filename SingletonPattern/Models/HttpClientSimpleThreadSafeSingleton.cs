namespace SingletonPattern.Models;

public sealed class HttpClientSimpleThreadSafeSingleton
{
    private static HttpClient? _httpClient;
    private static readonly object LockObject = new();

    public static HttpClient Instance
    {
        get
        {
            lock(LockObject) _httpClient ??= new();
            return _httpClient;
        }
    }
}