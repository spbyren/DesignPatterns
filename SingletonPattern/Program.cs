﻿using SingletonPattern.Models;

ShowResult(HttpClientSingleton.Instance, HttpClientSingleton.Instance);
ShowResult(HttpClientSimpleThreadSafeSingleton.Instance, HttpClientSimpleThreadSafeSingleton.Instance);
ShowResult(HttpClientThreadSafeSingleton.Instance, HttpClientThreadSafeSingleton.Instance);
ShowResult(HttpClientThreadSafeWithoutLock.Instance, HttpClientThreadSafeWithoutLock.Instance);

 void ShowResult(params HttpClient[] clients)
 {
     clients.ToList().ForEach(x => Console.WriteLine(x.GetHashCode()));
     Console.WriteLine();
 }